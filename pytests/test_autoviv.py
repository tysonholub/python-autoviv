import io
import json

import pytest
import simplejson

import autoviv
from pytests.helpers import sort_lists


def test_loads(list_str, dict_str, int_str, float_str, bool_str):
    data = autoviv.loads(list_str)
    assert isinstance(data, autoviv.List)
    assert data == json.loads(list_str)

    data = autoviv.loads(dict_str)
    assert isinstance(data, autoviv.Dict)
    assert data == json.loads(dict_str)

    data = autoviv.loads(int_str)
    assert isinstance(data, int)
    assert data == json.loads(int_str)

    data = autoviv.loads(float_str)
    assert isinstance(data, float)
    assert data == json.loads(float_str)

    data = autoviv.loads(bool_str)
    assert isinstance(data, bool)
    assert data == json.loads(bool_str)


def test_load(list_fp, dict_fp, list_data, dict_data):
    data = autoviv.load(list_fp)
    assert isinstance(data, autoviv.List)
    assert data == list_data

    data = autoviv.load(dict_fp)
    assert isinstance(data, autoviv.Dict)
    assert data == dict_data


def test_dumps(list_data, list_str, dict_data, dict_str):
    data = autoviv.parse(list_data)
    assert json.loads(autoviv.dumps(data)) == json.loads(list_str)

    data = autoviv.parse(dict_data)
    assert json.loads(autoviv.dumps(data)) == json.loads(dict_str)


def test_dump(list_str, dict_str):
    data = autoviv.loads(list_str)
    fp = io.StringIO()
    autoviv.dump(data, fp)
    fp.seek(0)
    assert json.loads(fp.read()) == json.loads(list_str)

    data = autoviv.loads(dict_str)
    fp = io.StringIO()
    autoviv.dump(data, fp)
    fp.seek(0)
    assert json.loads(fp.read()) == json.loads(dict_str)


def test_parse(list_data, dict_data, int_data, float_data, bool_data):
    data = autoviv.parse(list_data)
    assert isinstance(data, autoviv.List)
    assert data == list_data

    data = autoviv.parse(dict_data)
    assert isinstance(data, autoviv.Dict)
    assert data == dict_data

    data = autoviv.parse(int_data)
    assert isinstance(data, int)
    assert data == int_data

    data = autoviv.parse(float_data)
    assert isinstance(data, float)
    assert data == float_data

    data = autoviv.parse(bool_data)
    assert isinstance(data, bool)
    assert data == bool_data


def test_dict(dict_data):
    data = autoviv.Dict(dict_data)
    data_keys, dict_keys = sort_lists(data.keys(), dict_data.keys())
    assert data_keys == dict_keys

    for k in dict_data.keys():
        assert k not in dir(data)
        assert getattr(data, k) == dict_data[k]

    data = autoviv.Dict({})
    data.foo = {"bar": None}
    assert isinstance(data.foo, autoviv.Dict)
    assert data.foo.bar is None

    data.bar = [1, {"bar": None}]
    assert isinstance(data.bar, autoviv.List)
    assert data.bar[0] == 1
    assert isinstance(data.bar[-1], autoviv.Dict)

    data["foobar"] = {"foo": "bar"}
    assert isinstance(data.foobar, autoviv.Dict)
    assert data.foobar.foo == "bar"

    # this should blow up if something breaks simplejson
    simplejson.dumps(data)


def test_list(list_data):
    data = autoviv.List(list_data)
    assert len(data) == len(list_data)

    data.append({"foo": "bar"})
    assert isinstance(data[-1], autoviv.Dict)
    assert data[-1].foo == "bar"

    data.extend([{"bar": "foo"}, [1, 2, {"foo": "bar"}]])
    dct = data[-2]
    assert isinstance(dct, autoviv.Dict)
    assert dct.bar == "foo"
    lst = data[-1]
    assert isinstance(lst, autoviv.List)
    assert len(lst) == 3
    assert isinstance(lst, autoviv.List)
    assert 1 in lst
    assert 2 in lst
    assert isinstance(lst[-1], autoviv.Dict)
    assert lst[-1].foo == "bar"

    data.insert(1, "foo")
    assert data[1] == "foo"

    data[1] = [1, {}]
    assert isinstance(data[1], autoviv.List)
    assert isinstance(data[1][-1], autoviv.Dict)

    data[1] = {"foo": []}
    assert isinstance(data[1], autoviv.Dict)
    assert isinstance(data[1].foo, autoviv.List)

    simplejson.dumps(data)


def test_none_prop():
    data = autoviv.parse({})
    assert not data.missing_prop
    data.none_prop["test"] = "this"
    assert data.none_prop.test == "this"
    assert isinstance(data.prop, autoviv.NoneProp)
    assert "test" not in data.missing_prop
    assert [x for x in data.missing_prop] == []
    for _ in data.missing_prop:
        raise AssertionError("NoneProp should have nothing to yield")


def test_protected_methods():
    data = autoviv.Dict({k: k for k in dir(autoviv.Dict())})
    keys, dir_data = sort_lists(data.keys(), dir(data))
    assert keys == dir_data
    for k in keys:
        assert data[k] == k
        assert getattr(data, k) != k
        with pytest.raises(AttributeError) as e:
            setattr(data, k, "foo")
        assert str(e.value) == '"%s" is a protected method. Use key indexing to set attribute.' % k
        assert data.pop(k) == k
        data[k] = "foo"
        assert data[k] == "foo"


def test_pprint(dict_data):
    data = autoviv.Dict(dict_data)
    assert autoviv.pprint(data) == json.dumps(data, indent=4)
    assert autoviv.pprint(data, indent=None) == json.dumps(data, indent=None)


def test_digit_keys():
    data = autoviv.parse({})
    data[5] = 10
    assert data.get(5) == 10
