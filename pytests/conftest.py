import json
import os

import pytest

TEST_PATH = os.path.dirname(__file__)


@pytest.fixture
def list_fp():
    with open(os.path.join(TEST_PATH, "data/list.json")) as f:
        yield f


@pytest.fixture
def list_str(list_fp):
    data = list_fp.read().strip()
    list_fp.seek(0)
    return data


@pytest.fixture
def list_data(list_fp):
    data = json.load(list_fp)
    list_fp.seek(0)
    return data


@pytest.fixture
def dict_fp():
    with open(os.path.join(TEST_PATH, "data/dict.json")) as f:
        yield f


@pytest.fixture
def dict_str(dict_fp):
    data = dict_fp.read().strip()
    dict_fp.seek(0)
    return data


@pytest.fixture
def dict_data(dict_fp):
    data = json.load(dict_fp)
    dict_fp.seek(0)
    return data


@pytest.fixture
def int_str():
    return "1"


@pytest.fixture
def int_data(int_str):
    return int(int_str)


@pytest.fixture
def float_str():
    return "3.14"


@pytest.fixture
def float_data(float_str):
    return float(float_str)


@pytest.fixture
def bool_str():
    return "true"


@pytest.fixture
def bool_data(bool_str):
    return json.loads(bool_str)
