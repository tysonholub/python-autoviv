def sort_lists(*args):
    for lst in args:
        result = list(lst)
        result.sort()
        yield result
